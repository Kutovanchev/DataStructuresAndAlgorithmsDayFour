package taskOne;
import java.util.Stack;

public class DepthFirstTraversal {
	static Stack<Integer> stack = new Stack<>();
	boolean[][] adj;
	boolean visited[];
	int limit;

	public boolean traverse(Graph graph, int firstNum, int lastNum) {
		limit = graph.V();

		visited = new boolean[limit];
		for (@SuppressWarnings("unused")
		boolean b : visited) {
			b = false;
		}
		adj = graph.getAdjacencyMatrix();
		stack.push(lastNum);
		while (!stack.isEmpty()) {
			if (stack.peek() == firstNum)
				break;
			visit(stack.pop(), firstNum, lastNum);
		}

		for (boolean b : visited) {
			if (!b)
				return false;
		}
		return true;

	}

	public void visit(int vertex, int startNum, int endNum) {
		if (visited[vertex])
			return;
		
		System.out.println("Now visiting " + vertex);
		if (startNum < endNum) {
			for (int j = endNum; j >= startNum; j--) {
				if (adj[vertex][j]) {
					stack.push(j);

				}
				visited[vertex] = true;
			}
		} else {
			for (int j = startNum; j >= endNum; j--) {
				if (adj[vertex][j]) {
					stack.push(j);

				}
				visited[vertex] = true;
			}
		}
	}
}
