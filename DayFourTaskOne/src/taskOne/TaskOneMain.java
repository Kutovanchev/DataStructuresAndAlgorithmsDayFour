package taskOne;

public class TaskOneMain {

	public static void main(String[] args) {
		Graph graph = new Graph(7);
		graph.addEdge(0, 1);
		graph.addEdge(0, 2);
		graph.addEdge(0, 4);
		graph.addEdge(1, 2);
		graph.addEdge(1, 3);
		graph.addEdge(2, 3);
		graph.addEdge(3, 4);
		graph.addEdge(3, 5);
		graph.addEdge(3, 6);
		graph.addEdge(4, 6);
		graph.addEdge(5, 6);

		System.out.println(graph.toString());
		System.out.println(new DepthFirstTraversal().traverse(graph, 0, 6));

	}

}
