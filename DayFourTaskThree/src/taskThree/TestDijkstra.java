package taskThree;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class TestDijkstra {

	private List<Vertex> nodes;
	private List<Edge> edges;

	public void testExcute() {
		nodes = new ArrayList<Vertex>();
		edges = new ArrayList<Edge>();
		for (int i = 0; i < 11; i++) {
			Vertex location = new Vertex("Node_" + i, "Node_" + i);
			nodes.add(location);
		}

		addLane("Edge_0", 0, 1, 85, 20);
		addLane("Edge_1", 0, 2, 217, 10);
		addLane("Edge_2", 0, 4, 173, 15);
		addLane("Edge_3", 2, 6, 186, 17);
		addLane("Edge_4", 2, 7, 103, 5);
		addLane("Edge_5", 3, 7, 183, 12);
		addLane("Edge_6", 5, 8, 250, 7);
		addLane("Edge_7", 8, 9, 84, 6);
		addLane("Edge_8", 7, 9, 167, 16);
		addLane("Edge_9", 4, 9, 502, 4);
		addLane("Edge_10", 9, 10, 40, 22);
		addLane("Edge_11", 1, 10, 600, 42);

		// Lets check from location Loc_1 to Loc_10
		Graph2 graph = new Graph2(nodes, edges);
		DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(graph);
		dijkstra.execute(nodes.get(0));
		LinkedList<Vertex> path = dijkstra.getPath(nodes.get(10));
		LinkedList<Vertex> costPath = dijkstra.getPath(nodes.get(10));

		for (Vertex vertex : path) {
			System.out.println(vertex);
		}
		
		for (Vertex vertex : costPath) {
			System.out.println(vertex);
		}

	}

	private void addLane(String laneId, int sourceLocNo, int destLocNo, int duration, int cost) {
		Edge lane = new Edge(laneId, nodes.get(sourceLocNo), nodes.get(destLocNo), duration, cost);
		edges.add(lane);
	}
}