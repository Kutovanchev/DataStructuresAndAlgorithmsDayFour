package taskThree;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DijkstraAlgorithm {

    @SuppressWarnings("unused")
	private final List<Vertex> nodes;
    private final List<Edge> edges;
    private Set<Vertex> settledNodes;
    private Set<Vertex> unSettledNodes;
    private Map<Vertex, Vertex> predecessors;
    private Map<Vertex, Integer> distance;
    
    private Set<Vertex> settledNodes2;
    private Set<Vertex> unSettledNodes2;
    private Map<Vertex, Vertex> predecessors2;
    private Map<Vertex, Integer> distance2;

    public DijkstraAlgorithm(Graph2 graph) {
       
        this.nodes = new ArrayList<Vertex>(graph.getVertexes());
        this.edges = new ArrayList<Edge>(graph.getEdges());
    }

    public void execute(Vertex source) {
        settledNodes = new HashSet<Vertex>();
        unSettledNodes = new HashSet<Vertex>();
        distance = new HashMap<Vertex, Integer>();
        predecessors = new HashMap<Vertex, Vertex>();
        distance.put(source, 0);
        unSettledNodes.add(source);
        while (unSettledNodes.size() > 0) {
            Vertex node = getMinimum(unSettledNodes);
            settledNodes.add(node);
            unSettledNodes.remove(node);
            findMinimalDistances(node);
            findCheapestDistance(node);
        }
        
        settledNodes2 = new HashSet<Vertex>();
        unSettledNodes2 = new HashSet<Vertex>();
        distance2 = new HashMap<Vertex, Integer>();
        predecessors2 = new HashMap<Vertex, Vertex>();
        distance2.put(source, 0);
        unSettledNodes2.add(source);
        while (unSettledNodes2.size() > 0) {
            Vertex node = getMinimum(unSettledNodes2);
            settledNodes2.add(node);
            unSettledNodes2.remove(node);
            findMinimalDistances(node);
            findCheapestDistance(node);
        }
    }

    private void findMinimalDistances(Vertex node) {
        List<Vertex> adjacentNodes = getNeighbors(node);
        for (Vertex target : adjacentNodes) {
            if (getShortestDistance(target) > getShortestDistance(node)
                    + getDistance(node, target)) {
                distance.put(target, getShortestDistance(node)
                        + getDistance(node, target));
                predecessors.put(target, node);
                unSettledNodes.add(target);
            }
        }
    }
    
    private void findCheapestDistance(Vertex node) {
    	   List<Vertex> adjacentNodes = getNeighbors(node);
           for (Vertex target : adjacentNodes) {
               if (getCheapestDistance(target) > getCheapestDistance(node)
                       + getCost(node, target)) {
                   distance2.put(target, getCheapestDistance(node)
                           + getCost(node, target));
                   predecessors2.put(target, node);
                   unSettledNodes2.add(target);
               }
           }
    }

    private int getDistance(Vertex node, Vertex target) {
        for (Edge edge : edges) {
            if (edge.getSource().equals(node)
                    && edge.getDestination().equals(target)) {
                return edge.getWeight();
            }
        }
        throw new RuntimeException("Should not happen");
    }
    
    private int getCost(Vertex node, Vertex target) {
    	   for (Edge edge : edges) {
               if (edge.getSource().equals(node)
                       && edge.getDestination().equals(target)) {
                   return edge.getCost();
               }
           }
           throw new RuntimeException("Should not happen");
    }

    private List<Vertex> getNeighbors(Vertex node) {
        List<Vertex> neighbors = new ArrayList<Vertex>();
        for (Edge edge : edges) {
            if (edge.getSource().equals(node)
                    && !isSettled(edge.getDestination())) {
                neighbors.add(edge.getDestination());
            }
        }
        return neighbors;
    }

    private Vertex getMinimum(Set<Vertex> vertexes) {
        Vertex minimum = null;
        for (Vertex vertex : vertexes) {
            if (minimum == null) {
                minimum = vertex;
            } else {
                if (getShortestDistance(vertex) < getShortestDistance(minimum)) {
                    minimum = vertex;
                }
            }
        }
        return minimum;
    }

    private boolean isSettled(Vertex vertex) {
        return settledNodes.contains(vertex);
    }

    private int getShortestDistance(Vertex destination) {
        Integer d = distance.get(destination);
        if (d == null) {
            return Integer.MAX_VALUE;
        } else {
            return d;
        }
    }
    
    private int getCheapestDistance(Vertex destination) {
        Integer d = distance2.get(destination);
        if (d == null) {
            return Integer.MAX_VALUE;
        } else {
            return d;
        }
    }

   
    public LinkedList<Vertex> getPath(Vertex target) {
        LinkedList<Vertex> path = new LinkedList<Vertex>();
        Vertex step = target;
        if (predecessors.get(step) == null) {
            return null;
        }
        path.add(step);
        while (predecessors.get(step) != null) {
            step = predecessors.get(step);
            path.add(step);
        }
        Collections.reverse(path);
        return path;
    }
    
    public LinkedList<Vertex> getCostPath(Vertex target) {
        LinkedList<Vertex> path = new LinkedList<Vertex>();
        Vertex step = target;
        if (predecessors2.get(step) == null) {
            return null;
        }
        path.add(step);
        while (predecessors2.get(step) != null) {
            step = predecessors2.get(step);
            path.add(step);
        }
        Collections.reverse(path);
        return path;
    }

}
