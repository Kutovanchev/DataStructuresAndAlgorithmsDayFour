package taskTwo;
import java.util.Stack;
//Actual deptFirst
public class DepthFirstTraversal {
	static Stack<Integer> stack = new Stack<>();
	boolean[][] adj;
	boolean visited[];
	int limit;

	public boolean traverse(Graph graph, int firstNum, int lastNum) {
		limit = graph.V();

		visited = new boolean[limit];
		for (@SuppressWarnings("unused")
		boolean b : visited) {
			b = false;
		}
		adj = graph.getAdjacencyMatrix();
		
		visit(firstNum, firstNum, lastNum);		

		for (boolean b : visited) {
			if (!b)
				return false;
		}
		return true;

	}

	public void visit(int vertex, int startNum, int endNum) {
		

	
		System.out.println("Now visiting " + vertex);
		if (startNum < endNum && !visited[vertex]) {
			for (int j = endNum; j >= startNum; j--) {
				if (adj[vertex][j] && !visited[vertex]) {
					if(!visited[j]) {
						visit(j, j, endNum);
						visited[vertex] = true;
					}
					else {
						continue;
					}
				}
			}
			
		}else {
			return;
		}
	}
}
